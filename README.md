# hrms
 SSM（Spring + Spring MVC + MyBatis）框架 + easyUI 来开发一个比较简易的人事管理系统,

项目背景的设计参考自疯狂软件编著的《Spring+MyBatis 企业应用实战》
前端代码大多来自 ZHENFENG13 在 GitHub 上的开源项目 ssm-demo，其地址为 https://github.com/ZHENFENG13/ssm-demo，在该项目页面设计的基础上根据人事管理系统项目的功能需求等方面稍作修改并加以完善

